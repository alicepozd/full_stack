from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),

    path('auth/', include('djoser.urls')),
    path('auth/', include('djoser.urls.jwt')),
	
    path("api/accounts/",include("accounts.urls")),
    path("timetable/", include("timetable.urls")),
    path("forum/", include("forum.urls"))
]
