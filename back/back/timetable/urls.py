from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import *

urlpatterns = [
    path('get_timetable/', TimetableList.as_view()),
    path('add_event/', AddEvent.as_view())
]
