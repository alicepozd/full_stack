from django.db import models
from django.core.validators import MaxValueValidator
from django.core.validators import MinValueValidator

class Timetable(models.Model):
    name=models.CharField(max_length=36)
    day=models.IntegerField(
        validators=[
            MaxValueValidator(7),
            MinValueValidator(1)
        ])
    start_cell=models.IntegerField(
        validators=[
            MaxValueValidator(23),
            MinValueValidator(10)
        ])
    end_cell=models.IntegerField(
        validators=[
            MaxValueValidator(23),
            MinValueValidator(10)
        ])
    cost=models.IntegerField()

    def __str__(self):
        return self.name
