from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import *

urlpatterns = [
    path('get_all_comments/', CommentList.as_view()),
    path('add_comment/', AddComment.as_view())
]
