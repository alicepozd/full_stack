from django.shortcuts import render
from rest_framework.permissions import IsAuthenticated
from rest_framework import generics, serializers
from rest_framework.views import APIView

from .models import Comment
from .serializers import CommentSerializer


class CommentList(generics.ListAPIView):
    serializer_class = CommentSerializer
    queryset = Comment.objects.all()


class AddComment(generics.CreateAPIView):
        serializer_class = CommentSerializer
