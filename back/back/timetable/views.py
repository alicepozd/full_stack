from django.shortcuts import render
from rest_framework.permissions import IsAuthenticated
from rest_framework import generics, serializers
from rest_framework.views import APIView

from .models import Timetable
from .serializers import TimetableSerializer


class TimetableList(generics.ListAPIView):
    serializer_class = TimetableSerializer
    queryset = Timetable.objects.all()


class AddEvent(generics.CreateAPIView):
        serializer_class = TimetableSerializer
